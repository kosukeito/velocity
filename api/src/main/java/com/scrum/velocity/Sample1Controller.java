package com.scrum.velocity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Sample1Controller {

    @Data
    class HogeData {
        @JsonProperty("sample_id")
        private Integer id;
        @JsonProperty("sample_name")
        private String name;
    }



    @GetMapping(value = {"/sample1"})
    public ResponseEntity<List<HogeData>> getHoge() {
        List<HogeData> store = new ArrayList();
        HogeData hogeData = new HogeData();
        hogeData.id = 2;
        hogeData.name = "aiueo";
        store.add(hogeData);

        return new ResponseEntity<>(store, HttpStatus.OK);
    }

}
